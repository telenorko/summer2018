#ifndef NODE_H
#define NODE_H

#include <iostream>
using namespace std;

class node
{
public:
    node *left;
    node *right;
    node *prev;
    string otazka;
    string zvire;
    node(string a);
    ~node();
};

node::node(string a)
{
    this->otazka = a;
    this->zvire = "";
    this->left = NULL;
    this->right = NULL;
    this->prev = NULL;
}

node::~node()
{
    delete left;
    delete right;
    delete prev;
}

#endif // NODE_H
