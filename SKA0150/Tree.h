#ifndef TREE_H
#define TREE_H

string getNazev(string a)
{
    a.erase(0, 6);
    a.erase(a.size() - 1, a.size());
    return a;
}

#include <iostream>
using namespace std;

#include "Node.h"

class tree
{
public:
    static int pocet;
    node *root;
    tree(string a, string b, string c);
    ~tree();
    node *smazani(node *rooty);
    node *getRoot();
    node *menu(node *rooty);
    node *cojeto(node *tahle);
    void pokracovat();
    node *showTree(node *rooty);
};

int tree::pocet = 0;

node *tree::showTree(node *rooty)
{
    if (rooty != NULL)
    {
        cout << rooty << "  " << rooty->prev << "  " << rooty->left << "  " << rooty->right << "  " << rooty->otazka << endl;
        showTree(rooty->left);
        showTree(rooty->right);
    }
}

void tree::pokracovat()
{
    string ot;
    getline(cin, ot);
    cout << endl;
    if (ot == "Ano" || ot == "ano" || ot == "Jo" || ot == "jo" || ot == "1")
        menu(getRoot());
};

tree::~tree()
{
    //smazani(getRoot());
}

node *tree::cojeto(node *tahle)
{
    cout << "Jsem porazen! Co to bylo za zvire? ";

    string ot, ot2;
    getline(cin, ot);
    ot2 = "Je to " + ot + "?";

    node *tmp = new node(ot2);
    tmp->zvire = ot;

    cout << "Napis mi, prosim, otazku na kterou je pro " << ot << " odpoved ano a pro " << tahle->zvire << " ne: " << endl;

    string ot3;
    getline(cin, ot3);
    tmp->prev = new node(ot3);

    node *tmp2 = tmp->prev;
    tmp2->right = tmp;

    node *tmp3 = tahle;
    node *tmp4 = tahle->prev;

    if (tmp4->left == tmp3)
    {
        tmp4->left = tmp2;
    }
    else if (tmp4->right == tmp3)
    {
        tmp4->right = tmp2;
    }

    tmp3->prev = tmp2;
    tmp2->left = tmp3;
    tmp2->prev = tmp4;
}

node *tree::menu(node *rooty)
{
    if (rooty == this->root)
    {
        //showTree(getRoot());
        cout << "Mysli si zvire a ja budu hadat." << endl;
    }
    tree::pocet += 1;
    cout << rooty->otazka << " ";

    string odpoved;
    getline(cin, odpoved);
    if (odpoved == "Ano" || odpoved == "ano" || odpoved == "Jo" || odpoved == "jo" || odpoved == "1")
    {
        if (rooty->right != NULL)
        {
            menu(rooty->right);
        }
        else
        {
            cout << "Vyhral jsem! Pokracovat? ";
            pokracovat();
        }
    }
    else if (odpoved == "Ne" || odpoved == "ne" || odpoved == "Jo" || odpoved == "jo" || odpoved == "0")
    {
        if (rooty->left != NULL)
        {
            menu(rooty->left);
        }
        else
        {
            cojeto(rooty);
            cout << "Dekuji, a pokracovat? ";
            pokracovat();
        }
    }
}

tree::tree(string a, string b, string c)
{
    this->root = NULL;
    node *tmp = new node(a);

    tmp->left = new node(b);
    tmp->right = new node(c);

    b = getNazev(b);
    c = getNazev(c);

    tmp->left->zvire = b;
    tmp->right->zvire = c;

    this->root = tmp;

    node *tmp2 = this->root;
    tmp2 = tmp2->left;

    tmp2->prev = this->root;

    node *tmp3 = this->root;
    tmp3 = tmp3->right;

    tmp3->prev = this->root;

    menu(getRoot());
}

node *tree::smazani(node *rooty)
{
    if (rooty != NULL)
    {
        smazani(rooty->left);
        smazani(rooty->right);

        if (rooty->left == NULL && rooty->right == NULL)
            delete rooty;
    }
}

node *tree::getRoot()
{
    return this->root;
}


#endif // TREE_H
