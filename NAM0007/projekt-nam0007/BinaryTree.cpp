#include "stdafx.h"
#include "BinaryTree.h"

BinaryTree::BinaryTree()
{
	this->root = nullptr;
}

BinaryTree::BinaryTree(int value)
{
	this->Add(value);
}

BinaryTree::BinaryTree(int *valueArray, unsigned int valueCount)
{
	this->Add(valueArray,valueCount);
}

BinaryTree::~BinaryTree()
{
	delete this->root;
}



void BinaryTree::Add(int value)
{
	if (this->IsEmpty())
	{
		this->root = new Node(value);
		return;
	}


	Node *current = this->root;

	while (current != nullptr && current->value != value)
	{
		if (value < current->value && current->left == nullptr)
			current->left = new Node(value);
		else if (value < current->value)
			current = current->left;

		if (value > current->value && current->right == nullptr)
			current->right = new Node(value);
		else if (value > current->value)
			current = current->right;
	}
}

void BinaryTree::Add(int *valueArray, unsigned int valueCount)
{
	for (unsigned int i = 0; i < valueCount; i++)
	{
		this->Add(valueArray[i]);
	}
}



void BinaryTree::PrintRecursiveInorder(Node *node)
{
	if (node != nullptr)
	{
		PrintRecursiveInorder(node->left);
		cout << node->value << " ";
		PrintRecursiveInorder(node->right);
	}
}

void BinaryTree::PrintRecursiveInorder()
{
	this->PrintRecursiveInorder(this->root);
}



Node* BinaryTree::Search(int value)
{
	if (this->IsEmpty())
		return nullptr;

	Node *current = this->root;
	while (current != nullptr && current->value != value)
	{
		if (value < current->value)
		{
			current = current->left;
		}
		else
		{
			current = current->right;
		}
	}

	return current;
}



bool BinaryTree::IsEmpty()
{
	if (this->root == nullptr)
		return true;

	return false;
}



bool BinaryTree::CompareStructures(Node *a, Node *b)
{
	if (a == nullptr && b == nullptr)
		return true;

	if (!Node::CompareStructure(a, b))
		return false;
	
	bool leftTree = BinaryTree::CompareStructures(a->left, b->left);
	bool righTree = BinaryTree::CompareStructures(a->right, b->right);
	
	return leftTree && righTree;
}

bool BinaryTree::CompareStructures(BinaryTree &a, BinaryTree &b)
{
	return BinaryTree::CompareStructures(a.root, b.root);
}
