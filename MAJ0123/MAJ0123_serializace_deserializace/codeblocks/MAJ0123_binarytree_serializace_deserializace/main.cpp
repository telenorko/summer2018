/*****************************************************************************************************
 * 3 Serializ�tor bin�rn�ho stromu                                                                   *
 *                                                                                                   *
 * 3.1 Probl�m                                                                                       *
 *                                                                                                   *
 *   Implementujte program, kter� bude schopen serializovat a deserializovat bin�rn� vyhled�vac�     *
 *   strom. Serializace je p�evod objektu, v na�em p��pad� bin�rn�ho stromu, do tvaru, kter� lze     *
 *   ulo�it do souboru, p�en�st po s�ti apod. jako celek. Deserializace je samoz�ejm� proces p�esn�  *
 *   opa�n�, tedy rekonstrukce objekt z ulo�en�ch dat.                                               *
 *                                                                                                   *
 *                                                                                                   *
 * 3.2 Uk�zka                                                                                        *
 *                                                                                                   *
 *   M�jme n�sleduj�c� strom:                                                                        *
 *   Obr�zek 2: Strom p�ed serializac� (a z�rove� i po n�)                                           *
 *   Serializovan� tvar stromu pak m��e vypadat nap��klad n�sledovn�:                                *
 *   4;2;1;x;x;3;x;x;6;5;x;x;7;x;x                                                                   *
 *   kde x reprezentuje pr�zdn� strom � je nutn� toti� ulo�it i informaci, �e uzel ji� nem� n�kter�ho*
 *   nebo dokonce oba potomky. Jak� zp�sob pr�chodu stromem byl pou�it asi pozn�te sami.             *
 *                                                                                                   *
 *                                                                                                   *
 * 3.3 Implementace                                                                                  *
 *                                                                                                   *
 *   � Strom, kter� budete serializovat vytvo�te jako bin�rn� vyhled�vac� strom s kl��i typu int.    *
 *   Kl��e do stromu generujte n�hodn�.                                                              *
 *   � Zp�sob� serializace je velk� mno�stv�, jak� si vymysl�te je na V�s. V�echny jsou zalo�eny     *
 *   na algoritmu pro pr�chod stromem.                                                               *
 *   � Serializovanou podobu stromu ulo�te do souboru. Jestli bude textov� nebo bin�rn� je na        *
 *   V�s.                                                                                            *
 *   � Z disku pak serializovan� tvar na�t�te a deserializujte jej, abyste dostali p�vodn� strom.    *
 *   � Poskytn�te zp�sob, jak ov��it, �e deserializovan� strom je shodn� s t�m p�vodn�m. T�eba       *
 *   p�ehledn� v�pis na standardn� v�stup � testovac� stromy nemus� b�t velk�.                       *
 *****************************************************************************************************/

/**
 * @file main.cpp
 * @author Lukas Majoros
 * @date 20 Dubna 2018
 * @mainpage MAJ0123 Binarni strom - Serializace / deserializace
 *
 * Hlavni soubor projektu, obsahuje otevirani souboru, generovani / input cisel
 */

/*!
 *  Klasicke knihovny , C# , Cpp , pro generator, pro praci se soubory.
 */
#include <iostream>
#include <ctime>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <conio.h>
#include <fstream>
#include "BinaryTree.h"

using namespace std;

int main() {
    string file_name; /**< Deklarace promennych  */
    int number=0,sizeoftree=0,treenumber=0;/**< Deklarace promennych  */
    srand(time(nullptr));
	BinaryTree * tree = new BinaryTree(); /**< Vytvoreni noveho stromu  */


    ifstream ifs; /**< Ifstream pro praci se souborem  */
    ofstream ofs; /**< Ofstream pro zapis do souboru  */

    /**
 * @brief Volba vytvoreni stromu
 *
 * Uzivatel si muze vybrat mezi nahodnym vygenerovanim cisel ( pocet cisel / velikost stromu zadava uzivatel )
 * nebo manualne zadane cisla.
 */
    cout << " Zvolte zpusob vytvoreni stromu: " << endl;
    cout << "------------------------------------------------" << endl;
    cout << " 1) Zadani cisel manualne"<< endl;
    cout << " 2) Vygenerovani cisel" << endl;
    cout << "------------------------------------------------" << endl;
    cin >> number;

    /**
 * @brief Ofstream
 *
 * Prace se souborem,
 * otevreni souboru, podle inputu uzivatele se vybere akce, zadane / vygenerovane cisla se posilaji do funkce Insert.
 */

    cout << "Zadej jmeno souboru i s priponou ( eg. - data.txt / data.csv etc. )" << endl;
    cin >> file_name;
    ofs.open(file_name);
    if (number == 1)
    {
        cout << " Zadejte velikost stromu: " << endl;
        cin >> sizeoftree;
        cout << " Zadejte cisla: " << endl;
        for(int i = 0;i < sizeoftree;i++)
        {
            cin >> treenumber;
            tree->Insert(treenumber);
        }
    }
    else{
        cout << " Zadejte velikost stromu: " << endl;
        cin >> sizeoftree;
	for (int i = 0; i < sizeoftree; i++) {
		tree->Insert(rand() % 20 + 1);
	}
	}


	    /**
 * @brief Print
 *
 * Vola se funkce Print() , vytiskne se do konzole vygenerovany / zadany strom. ( Ve stejnem formatu, jako je v souboru.)
 */
    cout << "\n" << endl;
	cout << " Vytvoreny strom: " << endl;
	tree->Print();

    cout << "\n" << endl;
	cout << " Zapis stromu v souboru: ";
	cout << endl;
	string data = tree->Serialize();
    cout << ": " << data << "\n\n";
	ofs << data;
	ofs.close();

    data="";
    file_name="";


    	    /**
 * @brief Ifstream
 *
 * Pomoci getline nactu znaky do promenne data, tu nasledne posilam do funkce deserialize
 * Strom po deserializaci znovu vytisknu pomoci funkce Print()
 */
    cout << "Zadej jmeno souboru i s priponou ( eg. - data.txt / data.csv etc. )" << endl;
    cin >> file_name;
	ifs.open(file_name);

	getline(ifs,data);
	tree = BinaryTree::Deserialize(data);
	cout << "\n" << endl;
	cout << " Strom po deserializaci: " << endl;
	tree->Print();
	ifs.close();

	return 0;
}
