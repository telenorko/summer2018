#include "BinaryTree.h"
#include "Queue.h"
#include <iostream>
#include <sstream>


/**
 * @file BinaryTree.cpp
 * @author Lukas Majoros
 * @date 20 Dubna 2018
 * @brief cpp soubor tridy BinaryTree
 *
 * obsahuje strukturu Node
 * a funkce Insert, Clear, Serialize a deserialize
 */

  /****************************************************************************
 *   Trida BinaryTree je zakladem stejna jak ze cviceni,                    *
 *   pouze jsou implementovany metody Print, serializace a deserializace    *
 ****************************************************************************/




/**
 * @brief Konstruktor
 *
 * vynulovani korenu
 */
BinaryTree::BinaryTree() {
	root = nullptr;
}


/**
 * @brief Destruktor
 *
 * Smazani stromu
 */
BinaryTree::~BinaryTree() {
	Clear();
}

/**
 * @brief Funkce Insert
 *
 * Porovnava vstup, podle toho pridava cislo do stromu
 */
void BinaryTree::Insert(Node *& node, const int & value) {
	if (node == nullptr) {
		node = new Node();
		node->Key = value;
	} else if (node->Key != value) {
		if (value < node->Key)
			Insert(node->Left, value);
		else
			Insert(node->Right, value);
	}
}

/**
 * @brief Funkce Insert r.
 *
 * Rekurzivne volany insert
 */
void BinaryTree::Insert(const int & value) {
	Insert(root, value);
}

/**
 * @brief Funkce Clear
 *
 * Smazani stromu
 */
void BinaryTree::Clear(Node *& node) {
	if (node != nullptr) {
		Clear(node->Left);
		Clear(node->Right);
		delete node;
	}
}

/**
 * @brief Funkce clear
 *
 * Smazani stromu
 */
void BinaryTree::Clear() {
	Clear(root);
}

/**
 * @brief Funkce Print
 *
 * Vypis pomoci breadth-first
 */
void BinaryTree::Print() {
	Queue * queue = new Queue();
	queue->Enqueue(root);
	while (!queue->IsEmpty()) {
		Node * node = queue->Dequeue();
		cout << node->Key << "\n";

		if (node->Left != nullptr)
			queue->Enqueue(node->Left);
		if (node->Right != nullptr)
			queue->Enqueue(node->Right);
	}
}


/**
 * @brief Funkce Serialize
 *
 * Serializace pomoci breadth-first
 */
string BinaryTree::Serialize() {
	Queue * queue = new Queue();
	queue->Enqueue(root);
	stringstream ss;
	while (!queue->IsEmpty()) {
		Node * node = queue->Dequeue();
		ss << node->Key;

		if (node->Left != nullptr)
			queue->Enqueue(node->Left);
		if (node->Right != nullptr)
			queue->Enqueue(node->Right);

		if (!queue->IsEmpty())
			ss << ";";
	}

	return ss.str();
}



/**
 * @brief Funkce Serialize
 *
 * Ocekavame, ze data budou ulozeny pomoci breadth-first, tudiz muzeme nacist postupne vsechny hodnoty a funkce Insert nam je opet seradi.
 */
BinaryTree * BinaryTree::Deserialize(string data) {
	BinaryTree * tree = new BinaryTree();
	istringstream iss(data);
	int num;
	while (!iss.eof()) {
		iss >> num;
		tree->Insert(num);
		iss.get();
	}

	return tree;
}
