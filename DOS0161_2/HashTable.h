#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <functional>

#define HASH_TAB vector <vector <string>>

using namespace std;

class HashTable {
	int size;
	HASH_TAB table;
public:
	HashTable();
	HashTable(int Size);
	~HashTable();

	void insert(string s);
	bool search(string s);
	int count(); //TODO : vracej�c� ��slo typu int odpov�daj�c� po�tu prvk� ulo�en�ch v tabulce
	double loadFactor(); //TODO :  vracej�c� ��slo typu double odpov�daj�c� faktoru napln�n� tabulky

	int getMaxSize();

	void report();
};