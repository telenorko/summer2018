# ALG2 - Project 1

### Author: Josef Dostál

### Project group: 1

### Task: 2

### Details:
##### Setříděný seznam
Máte k dispozici obousměrný seznam implementovaný pomocí spojových struktur, zdrojový
kód je uveden v přílohách A a B. Implementace je dále dostupná na http://www.cs.vsb.cz/dvorsky/Download/ALGII/ProjectI/List.zip.
Vaším úkolem je upravit implementaci tak, aby byl seznam neustále vzestupně setříděn, tj.
prvky v něm budou vzestupně setříděny. Seznam budeme považovat za vzestupně setříděný,
pokud jeho hlava ukazuje na nejmenší prvek v seznamu a ocas na největší prvek v seznamu.
Při úpravách zvažte následující:
* Jaké úpravy bude nutné provést ve funkcích Init?
* Jaké úpravy bude nutné provést ve funkcích pro vkládání prvků – Prepend, Append,
InsertBefore a InsertAfter? Jaké by měly mít tyto funkce chování? Mají v případě setříděného
seznamu vůbec smysl? Nelze je nahradit jedinou funkcí?
* Bylo by možné ve funkcích Search a ReverseSearch využít toho, že se prohledávaný
seznam uspořádaný a napsat o něco málo efektivnější vyhledávací algoritmus? Prozradím
rovnou, že se jedná o modifikaci stávajícího algoritmu, nebude určitě jednat o vyhledávání
půlením intervalu.
